# Natural Language Processing: Trump/Orwell
In diesem Programm wird die Worthäufigkeit in verschiedenen Texten geprüft und visualisiert.

## Pogrammablauf
Zu Beginn wird eine Textdatei eingelesen. Anschließend wird der Text in Kleinbuchstaben umgewandelt. Danach wird eine Liste einzelner Wörter mit einem *Tokeniser* erstellt, der auch die Satzzeichen entfernt. Die Wortliste wird dann von einem *Stemmer* verarbeitet, um verschiedene grammatikalische Formen unter einem gemeinsamen Pseudo-Wortstamm (*stem*) zusammenzufassen. Aus dieser Liste werden noch die Stopwords (v. a. Bindewörter) entfernt und der Rest in ein Dictionay umgeformt. Dann wird das Dictionary mit *pandas* in ein Dataframe konvertiert, um mit Matplotlib ein einfaches Diagramm zu erstellen.

## Verwendete Bibliotheken
* [nltk](https://www.nltk.org)
* [pandas](https://pandas.pydata.org)
* [matplotlib](https://matplotlib.org)

## Quellen
* [Donald Trump's full inauguration speech ](https://globalnews.ca/news/3194820/donald-trump-inauguration-speech-and-transcript/)
* [George Orwell – 1984](https://archive.org/details/Orwell1984preywo)