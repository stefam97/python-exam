#Ermitteln häufigstes Wort
#impot toolkit natural laguage (for stopwords) 
import nltk
import string
stopwords = set(nltk.corpus.stopwords.words('english'))

# Einlesen von .txt :
print('Welcher Text soll analysiert werden? [T]rump / [O]rwell')
auswahl = str(input('?: ')).upper()
if (auswahl == 'T'):
    f = open('Donald-Trump-inaugural-speech.txt')
elif (auswahl == 'O'):
    f = open('orwell.txt')
else:
    print('Ungültige Eingabe.')
txt = f.read()

# Alles in Kleinbuchstaben umwandeln
txt = txt.lower()

# Tokenisierung mit NLTK, Punktierung entfernen
# Regulärer Ausdruck "\w+": matches any word character ("\w") between one and unlimited times ("+")
my_regex_tokenizer = nltk.tokenize.RegexpTokenizer(r'\w+')
woerter = my_regex_tokenizer.tokenize(txt)

# Stemming
from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer('english')
# Ganze Liste durchgehen und ersetzen
for x in range(len(woerter)):
  woerter[x] = stemmer.stem(woerter[x])


# Einzelne Wörter zählen mit Hilfe eines Dictionarys
word_count = {}
for wort in woerter:
    if len(wort) > 0 and wort not in stopwords:
        word_count[wort] = word_count.get(wort, 0) + 1

# gezählte Wörer
# Dictionary für häufigstets Wort
haeufigstes_wort = ''
anzahl_vorkommen = 0
for wort in word_count:
    if word_count[wort] > anzahl_vorkommen:
        haeufigstes_wort = wort
        anzahl_vorkommen = word_count[wort]

print("Das haeufigste Wort ist '%s' mit %i Vorkommen." % (haeufigstes_wort, anzahl_vorkommen))

# Prep Visualisierung in Diagramm
import pandas as pd
# Dataframe aus dict
df_word_count = pd.DataFrame(list(word_count.items()), columns=['word', 'count'])
print(df_word_count.describe())
# Sortiere Spalte nach Wert
df_word_count.sort_values(['count'], axis=0, ascending=True, inplace=True) 
print(df_word_count.tail())
df_word_count = df_word_count.tail(n=20)

# Plot
import matplotlib.pyplot as plt
plt.barh(df_word_count['word'], df_word_count['count'])
plt.show()

 